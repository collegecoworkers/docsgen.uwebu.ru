<?php 
$main_info = array(
	array(
		'label' => '',
		'fields' => array(
			array('type'=>'input', 'label'=>'Название продукта', 'placehold'=>'', 'name'=>'one'),
			array('type'=>'input', 'label'=>'Назначение продукта (цель создания)', 'placehold'=>'', 'name'=>'two'),
			array('type'=>'input', 'label'=>'Язык интерфейса системы ', 'placehold'=>'', 'name'=>'three'),
			array('type'=>'input', 'label'=>'Целевые платформы ', 'placehold'=>'', 'name'=>'four'),
			array('type'=>'input', 'label'=>'Предполагаемая возрастная аудитория продукта', 'placehold'=>'', 'name'=>'five'),
			array('type'=>'input', 'label'=>'Ключевые слова описания системы ', 'placehold'=>'', 'name'=>'six'),
			array('type'=>'textarea', 'rows' => 5, 'label'=>'Количество модулей продукта ', 'placehold'=>'', 'name'=>'seven'),
			array('type'=>'textarea', 'rows' => 5, 'label'=>'Навигация и интерфейс системы ', 'placehold'=>'', 'name'=>'eight'),
			array('type'=>'textarea', 'rows' => 5, 'label'=>'Функциональные модули системы ', 'placehold'=>'', 'name'=>'nine'),
			array('type'=>'textarea', 'rows' => 5, 'label'=>'Проведение рекламной кампании (раскрутка)', 'placehold'=>'', 'name'=>'ten'),
			array('type'=>'textarea', 'rows' => 5, 'label'=>'Срок разработки продукта ', 'placehold'=>'', 'name'=>'eleven'),
			array('type'=>'textarea', 'rows' => 5, 'label'=>'Порядок передачи продукта ', 'placehold'=>'', 'name'=>'twelve'),
			array('type'=>'textarea', 'rows' => 5, 'label'=>'Сопровождение системы ', 'placehold'=>'', 'name'=>'thirteen'),
			array('type'=>'textarea', 'rows' => 5, 'label'=>'Дополнительные условия ', 'placehold'=>'', 'name'=>'fourteen'),
		)
	),
);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Генерация технической документации</title>
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no">
	<link rel="stylesheet" href="asssets/bootstrap.min.css">
</head>
<body>
	<div class="container">
		<!-- Static navbar -->
		<nav class="navbar navbar-default">
			<div class="container-fluid">
				<div class="navbar-header">
					<a class="navbar-brand" href="">Генерация технической документации</a>
				</div>
			</div>
		</nav>
		<h3>Заполните данные</h3>
		<form action="generate.php" method="post">
			<?php foreach ($main_info as $g_item): ?>
				<div class="row">
					<div class="col-md-12">
						<legend><?= $g_item['label']?></legend>
					</div>
					<div class="col-md-6 col-md-push-6">
						<div class="section_description"></div>
					</div>
					<div class="col-md-6 col-md-pull-6">
						<?php foreach ($g_item['fields'] as $item): ?>
							<?php if($item['type'] == 'input'): ?>
								<div class="form-group ">
									<label class="control-label"><?= $item['label']?></label>
									<input type="text" class="form-control" placeholder="<?= $item['placehold']?>" name="<?= $item['name']?>" value="" required>
								</div>
							<?php elseif($item['type'] == 'select'): ?>
								<div class="form-group ">
									<label class="control-label"><?= $item['label']?></label>
									<select name="<?= $item['name']?>" class="form-control">
										<?php foreach ($item['options'] as $key => $value): ?>
											<option value="<?= $value ?>"><?= $key ?></option>
										<?php endforeach ?>
									</select>
								</div>
							<?php elseif($item['type'] == 'textarea'): ?>
								<div class="form-group ">
									<label class="control-label"><?= $item['label']?></label>
									<textarea class="form-control" rows="<?= $item['rows']?>" placeholder="<?= $item['placehold']?>" name="<?= $item['name']?>" required></textarea>
								</div>
							<?php endif ?>
						<?php endforeach ?>
					</div>
				</div>
			<?php endforeach ?>
			<div class="row">
				<div class="col-md-12">
					<button type="submit" class="btn btn-success">Отправить</button>
				</div>
			</div>
		</form>
		<br><br><br><br><br>
	</div>
</body>
</html>