<?php 
require_once 'dompdf/autoload.inc.php';

use Dompdf\Dompdf;

function renderPage($path = null, $data = null) {
	if (!is_null($data)) { 
		extract($data);
	}
	ob_start();
	include($path);
	$var=ob_get_contents(); 
	ob_end_clean();
	return $var;
}

$dompdf = new Dompdf();
$dompdf->loadHtml(renderPage('tz.phtml', [
	'one' => $_POST['one'], 
	'two' => $_POST['two'], 
	'three' => $_POST['three'], 
	'four' => $_POST['four'], 
	'five' => $_POST['five'], 
	'six' => $_POST['six'], 
	'seven' => $_POST['seven'], 
	'eight' => $_POST['eight'], 
	'nine' => $_POST['nine'], 
	'ten' => $_POST['ten'], 
	'eleven' => $_POST['eleven'], 
	'twelve' => $_POST['twelve'], 
	'thirteen' => $_POST['thirteen'], 
	'fourteen' => $_POST['fourteen'], 
]));

$dompdf->setPaper('A4', '');
$dompdf->render();


$dompdf->stream('my tz.pdf', array('Attachment'=>0));